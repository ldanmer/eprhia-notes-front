import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {Observable, throwError} from 'rxjs';
import {Note, Order, Temple} from './temples-list/temple';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private templesUrl = `${environment.backendUrl}/api/`;
  private postHeaders = new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  });

  constructor(private http: HttpClient) {
  }

  getTemples(): Observable<string | Temple[]> {
    return this.http.get<Temple[]>(this.templesUrl).pipe(
      catchError(this.handleError)
    );
  }

  sendNote(note: Note): Observable<Note | ArrayBuffer> {
    return this.http.post<Note>(this.templesUrl, note, {
      headers: this.postHeaders
    }).pipe(
      catchError(this.handleError)
    );
  }

  getYKToken(order: Order): Observable<string> {
    // @ts-ignore
    return this.http.post<Order>(`${this.templesUrl}ykassa.php`, order, {
      headers: this.postHeaders
    });
  }

  private handleError(err): Observable<never> {
    let errorMessage: string;
    if (err.error instanceof ErrorEvent) {
      errorMessage = `${err.error.message}`;
    } else {
      errorMessage = `Server error: ${JSON.stringify(err.error)}`;
    }
    return throwError(errorMessage);
  }
}
