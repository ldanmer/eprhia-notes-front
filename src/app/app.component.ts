import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {Temple} from './temples-list/temple';
import {AppService} from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = `Подать записку о молитве в храмы<br>и монастыри Тверской епархии`;
  templeData$: Observable<string | Temple[]>;
  selectedTemple: Temple;

  constructor(private service: AppService) {
    this.templeData$ = this.service.getTemples();
  }

  selectTemple(temple: Temple): void {
    this.selectedTemple = temple;
    document.querySelector('main').scrollIntoView({block: 'start', behavior: 'smooth'});
  }
}
