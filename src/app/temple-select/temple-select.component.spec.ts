import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TempleSelectComponent } from './temple-select.component';

describe('TempleSelectComponent', () => {
  let component: TempleSelectComponent;
  let fixture: ComponentFixture<TempleSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TempleSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TempleSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
