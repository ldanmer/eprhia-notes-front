import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Temple} from '../temples-list/temple';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-temple-select',
  templateUrl: './temple-select.component.html',
  styleUrls: ['./temple-select.component.scss']
})
export class TempleSelectComponent implements OnInit {

  temples: Temple[];
  monasteries: Temple[];

  categoryFilterTitle = 'монастыри';

  constructor(
    public dialogRef: MatDialogRef<TempleSelectComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Observable<Temple[]>
  ) {
  }

  ngOnInit(): void {
    this.data.subscribe(
      temples => {
        this.temples = temples.filter(temple => temple.category !== this.categoryFilterTitle);
        this.monasteries = temples.filter(temple => temple.category === this.categoryFilterTitle);
      }
    );
  }

  templeClickHandler(temple: Temple): void {
    this.dialogRef.close(temple);
  }
}
