import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {Temple} from './temple';
import {Observable} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {TempleSelectComponent} from '../temple-select/temple-select.component';

@Component({
  selector: 'app-temples-list',
  templateUrl: './temples-list.component.html',
  styleUrls: ['./temples-list.component.scss']
})
export class TemplesListComponent implements OnInit, OnDestroy {
  @Output() selectItemEvent = new EventEmitter<Temple>();
  @Input() templeData$: Observable<Temple[]>;
  topTemples: Temple[];
  componentActive = true;
  templeData: Temple[];
  title = 'Где попросить о молитве?';
  showAllTemples = 'Посмотреть все храмы и монастыри';

  constructor(public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.templeData$.subscribe(
      top => {
        this.templeData = top;
        return this.topTemples = top.splice(0, 6);
      }
    );
  }

  onTempleSelect(temple: Temple): void {
    this.selectItemEvent.emit(temple);
  }

  ngOnDestroy(): void {
    this.componentActive = false;
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TempleSelectComponent, {
      width: '100%',
      data: this.templeData$
    });

    dialogRef.afterClosed().subscribe(temple => this.onTempleSelect(temple));
  }
}
