export interface Temple {
  id: number;
  name: string;
  image: string;
  category: string;
}

export interface Note {
  TEMPLE: string;
  NAMES: string;
}

export interface Order {
  price: number;
  description: string;
}
