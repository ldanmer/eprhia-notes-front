import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplesListComponent } from './temples-list.component';

describe('TemplesListComponent', () => {
  let component: TemplesListComponent;
  let fixture: ComponentFixture<TemplesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
