import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TemplesListComponent} from './temples-list/temples-list.component';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatListModule} from '@angular/material/list';
import {WriteANameComponent} from './write-a-name/write-a-name.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {CardComponent} from './write-a-name/card/card.component';
import {SendPropositionComponent} from './write-a-name/send-proposition/send-proposition.component';
import {HttpClientModule} from '@angular/common/http';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {YandexKassaComponent} from './write-a-name/yandex-kassa/yandex-kassa.component';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {TempleSelectComponent} from './temple-select/temple-select.component';
import {MatTabsModule} from '@angular/material/tabs';


@NgModule({
  declarations: [
    AppComponent,
    TemplesListComponent,
    WriteANameComponent,
    CardComponent,
    SendPropositionComponent,
    YandexKassaComponent,
    TempleSelectComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatFormFieldModule,
        FormsModule,
        MatDialogModule,
        MatButtonModule,
        MatInputModule,
        MatRadioModule,
        MatListModule,
        MatGridListModule,
        ReactiveFormsModule,
        HttpClientModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule,
        MatTabsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
