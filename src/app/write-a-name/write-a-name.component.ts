import {Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {Order, Temple} from '../temples-list/temple';
import {CardComponent} from './card/card.component';
import {AppService} from '../app.service';
import {SendPropositionComponent} from './send-proposition/send-proposition.component';

@Component({
  selector: 'app-write-a-name',
  templateUrl: './write-a-name.component.html',
  styleUrls: ['./write-a-name.component.scss'],
})
export class WriteANameComponent implements OnInit {

  @Input() selectedTemple: Temple;
  @ViewChildren(CardComponent) card: QueryList<CardComponent>;
  @ViewChild(SendPropositionComponent) sendComponent: SendPropositionComponent;
  @ViewChild('health', {read: ElementRef}) health: ElementRef;
  @ViewChild('dead', {read: ElementRef}) dead: ElementRef;
  order: Order;
  healthType: string;
  deadType: string;
  title = 'О ком будем молиться?';

  constructor(private service: AppService) {
  }

  ngOnInit(): void {
  }

  onSelectHealth($event: string): void {
    this.healthType = $event;
  }

  onSelectDead($event: string): void {
    this.deadType = $event;
  }

  submitForm(): void {
    this.card.map(component => {
      const firstInputName = component.type ? 'true-0' : 'false-0';
      if (!!component.noteForm.get(firstInputName).value) {
        const title = component.type ? 'О здравии' : 'О упокоении';
        const mailTemplate = this._createMailTemplate(component, title);
        this.service.sendNote({
          NAMES: mailTemplate,
          TEMPLE: this.selectedTemple.name
        }).subscribe(
          () => {
            this.order = {
              price: this.sendComponent.priceForm.get('price').value,
              description: `${new Date().toString()} - ${title}`
            };
          },
          error => console.error(error)
        );
      } else {
        document.querySelector('main').scrollIntoView({block: 'start', behavior: 'smooth'});
        document.querySelector('app-card').querySelector('input').focus();
      }
    });
  }

  _createMailTemplate(component, title): string {
    const form = component.noteForm;
    let namesList = '';
    for (const [key, value] of Object.entries(form.value)) {
      if (key !== 'radio-group' && !!value) {
        namesList += `<li>${value}</li>`;
      }
    }
    return `<h2>${title}</h2><h3>${form.get('radio-group').value}</h3><ul>${namesList}</ul>`;
  }

  healthShowClick($event: MouseEvent): void {
    this.visibilityToggle($event, this.health, this.dead);
  }

  deadShowClick($event: MouseEvent): void {
    this.visibilityToggle($event, this.dead, this.health);
  }

  private visibilityToggle($event: MouseEvent, firstComponent: ElementRef, secondComponent: ElementRef): void {
    firstComponent.nativeElement.style.display = 'block';
    secondComponent.nativeElement.style.display = 'none';
    const currentButton = $event.currentTarget as HTMLElement;
    const sibling = currentButton.nextElementSibling ? currentButton.nextElementSibling : currentButton.previousElementSibling;

    currentButton.classList.remove('mat-stroked-button');
    currentButton.classList.add('mat-raised-button');
    sibling.classList.remove('mat-raised-button');
    sibling.classList.add('mat-stroked-button');

  }
}
