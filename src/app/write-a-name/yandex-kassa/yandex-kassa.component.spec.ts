import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YandexKassaComponent } from './yandex-kassa.component';

describe('YandexKassaComponent', () => {
  let component: YandexKassaComponent;
  let fixture: ComponentFixture<YandexKassaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YandexKassaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YandexKassaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
