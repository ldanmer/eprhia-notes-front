import {Component, Input, OnInit} from '@angular/core';
import {AppService} from '../../app.service';
import {Order} from '../../temples-list/temple';

@Component({
  selector: 'app-yandex-kassa',
  templateUrl: './yandex-kassa.component.html',
  styleUrls: ['./yandex-kassa.component.scss']
})
export class YandexKassaComponent implements OnInit {

  @Input() order: Order;
  checkout: any;
  loader = true;

  constructor(private service: AppService) {
  }

  ngOnInit(): void {
    this.service.getYKToken(this.order).subscribe(
      token => {
        // @ts-ignore
        this.checkout = new window.YandexCheckout({
          confirmation_token: token,
          return_url: 'https://tvereparhia.ru',
          error_callback(error): void {
            console.error(error);
          }
        });
        this.checkout.render('payment-form');
        this.loader = false;
      },
      error => console.error(error)
    );
  }

}
