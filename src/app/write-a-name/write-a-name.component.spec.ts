import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WriteANameComponent } from './write-a-name.component';

describe('WriteANameComponent', () => {
  let component: WriteANameComponent;
  let fixture: ComponentFixture<WriteANameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WriteANameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WriteANameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
