import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() type: boolean;
  @Input() submitted: boolean;
  @Output() selectHealthEvent = new EventEmitter<string>();
  @Output() selectDeadEvent = new EventEmitter<string>();
  numberArray = Array(10);
  noteForm: FormGroup;
  radioGroup = [
    'Литургия',
    'Сорокоуст',
  ];

  constructor() {
  }

  ngOnInit(): void {
    this.radioGroup.push(this.type ? 'Молебен' : 'Панихида');
    this.noteForm = this.createForm();
    this.noteForm.addControl('radio-group', new FormControl('', Validators.required));
  }

  private createForm(): FormGroup {
    const group: any = {};
    for (let i = 0; i < this.numberArray.length; i++) {
      group[`${this.type}-${i}`] = new FormControl('');
    }
    return new FormGroup(group);
  }

  onRadioCheck(): void {
    const radioValue = this.noteForm.get('radio-group').value;
    const validatorsForInput = [Validators.minLength(2), Validators.required];
    this.type
      ? this.selectHealthEvent.emit(radioValue)
      : this.selectDeadEvent.emit(radioValue);
    this.type
      ? this.noteForm.get('true-0').setValidators(validatorsForInput)
      : this.noteForm.get('false-0').setValidators(validatorsForInput);
    setTimeout(() => {
      document.querySelector('app-send-proposition').scrollIntoView({block: 'start', behavior: 'smooth'});
    }, 0);
  }
}
