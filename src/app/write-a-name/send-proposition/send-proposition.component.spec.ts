import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendPropositionComponent } from './send-proposition.component';

describe('SendPropositionComponent', () => {
  let component: SendPropositionComponent;
  let fixture: ComponentFixture<SendPropositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendPropositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendPropositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
