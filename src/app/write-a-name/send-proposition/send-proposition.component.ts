import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Temple} from '../../temples-list/temple';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-send-proposition',
  templateUrl: './send-proposition.component.html',
  styleUrls: ['./send-proposition.component.scss']
})
export class SendPropositionComponent implements OnInit {

  @Input() selectedTemple: Temple;
  @Input() healthType: string;
  @Input() deadType: string;
  @Output() submitProposition = new EventEmitter<void>();
  priceForm: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.priceForm = this.fb.group({
      price: ['50', [Validators.required, Validators.min(50)]]
    });
  }

  sendProposition(): void {
    if (this.priceForm.valid) {
      this.submitProposition.emit();
    }
  }
}
